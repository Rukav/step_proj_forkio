# STEP PROGECT FORKIO

Developers: Ahruts Yelyzaveta, Serhii Rukavchenko
Git repository: https://gitlab.com/Rukav/step_proj_forkio
Git pages link: https://ahruts.gitlab.io/forkio/
Used tools: SCSS, Gulp, fontawesome

Ahruts Yelyzaveta is responsible for sections: header, intro, Revolutionary Editor
Serhii Rukavchenko is responsible for sections: Revolutionary Editor, Here is what you get, Fork Subscription Pricing